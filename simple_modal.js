(function ($) {
    "use strict";

    $.fn.heightCenteringSimpleModal = (contentBlock) => {
        function toggleCentering() {
            const contentHeight = contentBlock.outerHeight();
            const windowHeight = $(window).outerHeight();
            if (windowHeight <= contentHeight) {
                contentBlock.addClass('simple_modal__disable_height_centering');
            } else {
                contentBlock.removeClass('simple_modal__disable_height_centering');
            }
        }
        $(window).resize(toggleCentering).resize();
    };

    $.fn.closeSimpleModal = () => {
        $('#simple_modal_layout').remove();
        $('body').removeClass('simple_modal_opened');
        return false;
    };

    $.fn.simpleModal = function(options) {
        const settings = $.extend({
            closeByEscape: true,
            showCloseIcon: true,
            callback: undefined
        }, options);

        const buildSimpleModal = (e) => {
            const contentSelector = $(this).data('content');
            const content = $(contentSelector);
            const contentHeight = content.height();
            const closeIcon = settings.showCloseIcon ? '<a href="#" id="simple_modal_close" class="simple_modal__close"></a>' : '';
            const modalLayout = $(`<div id="simple_modal_layout" class="simple_modal">
                                        <div class="simple_modal__content">
                                            <div class="simple_modal__wrap">
                                                ${closeIcon}
                                            </div>
                                        </div>
                                    </div>`);
            $('body').addClass('simple_modal_opened').append(modalLayout);

            const clonedContent = content.clone();
            clonedContent.show();
            modalLayout.find('.simple_modal__wrap').prepend(clonedContent);
            modalLayout.find('#simple_modal_close').click($.fn.closeSimpleModal);
            $.fn.heightCenteringSimpleModal(modalLayout.find('.simple_modal__content'));
            $('body').on('keyup', (e) => {
                if (e.which !== 27 || !settings.closeByEscape) return;
                $.fn.closeSimpleModal();
            });
            if (typeof settings.callback === 'function') {
                settings.callback(e, clonedContent);
            }
        };

        this.on('click.simple_modal', (e) => {
            buildSimpleModal(e);
            return false;
        });
        return this;
    };

}(jQuery));

