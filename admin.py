# encoding: utf-8
from __future__ import unicode_literals

from mptt.admin import MPTTModelAdmin

from django.contrib import admin
from django import forms
from django.core.urlresolvers import reverse
from django.contrib.admin.views.main import ChangeList

from repair.models import RepairCompany, ServiceCategory, RepairApplication, RepairComplaint



class RepairCompanyAdminForm(forms.ModelForm):
    class Meta:
        model = RepairCompany
        exclude = ('time_be_first_expired', 'be_first_activated', 'host_added', 'ip', 'site_region')


class RepairCompanyAdmin(admin.ModelAdmin):
    form = RepairCompanyAdminForm
    list_per_page = 20
    raw_id_fields = ('user',)
    list_display = ('blocked', 'user_info', 'title', 'address', 'time_registered', 'time_updated', 'get_2gis_link')
    list_display_links = ('title',)
    list_filter = ('blocked',)
    search_fields = ('user__username', 'user__id', 'title', 'address')
    actions = ['disable_companies', 'enable_companies']

    def get_2gis_link(self, obj):
        if not obj.dgis_id:
            return u"Данные введены вручную"
        return '<a href="http://2gis.ru/krasnoyarsk/firm/%s">%s</a>' % (obj.dgis_id, obj.dgis_id)
    get_2gis_link.short_description = '2GIS'
    get_2gis_link.allow_tags = True

    def user_info(self, obj):
        return unicode(obj.user.id)
    user_info.short_description = u'Пользователь'

    def update_blocked_state(self, request, queryset, blocked):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        if not selected:
            return
        # NOTE: чтобы срабатывал перезаписанный метод save,
        #       вместо update будем использовать цикл и save()
        for company in queryset:
            company.blocked = blocked
            company.save()

    def disable_companies(self, request, queryset):
        self.update_blocked_state(request, queryset, True)
    disable_companies.short_description = u'Заблокировать'

    def enable_companies(self, request, queryset):
        self.update_blocked_state(request, queryset, False)
    enable_companies.short_description = u'Разблокировать'

    def get_changelist(self, request, **kwargs):
        return RepairChangeList

    class Media:
        css = {
            'all': ('css/admin.css',)
        }


class RepairChangeList(ChangeList):
    def get_queryset(self, request):
        qs = super(RepairChangeList, self).get_queryset(request)
        qs.query.distinct = False
        return qs


class ServiceCategoryTreeAdmin(MPTTModelAdmin):
    search_fields = ('title',)


class RepairApplicationAdmin(admin.ModelAdmin):
    raw_id_fields = ('user', 'category')


def block_company(modeladmin, request, queryset):
    for complaint in queryset:
        complaint.company.blocked = True
        complaint.company.save()
block_company.short_description = u"Заблокировать компанию"


def delete_complaint(modeladmin, request, queryset):
    for complaint in queryset:
        complaint.delete()
delete_complaint.short_description = u" Удалить жалобу"


class RepairComplaintAdmin(admin.ModelAdmin):
    raw_id_fields = ('author', 'company')
    list_display = ('__unicode__', 'company_link', 'author_ip', 'get_author', 'created')
    actions = [delete_complaint, block_company,]

    def get_author(self, obj):
        return obj.author if obj.author else 'Anonymous'

    def company_link(self, obj):
        company_uri = reverse('admin:repair_repaircompany_change', args=[obj.company.pk])
        return '<a href="%s">%s</a>' % (company_uri, obj.company.title)
    company_link.short_description = u'Компания'
    company_link.allow_tags = True


admin.site.register(ServiceCategory, ServiceCategoryTreeAdmin)
admin.site.register(RepairCompany, RepairCompanyAdmin)
admin.site.register(RepairApplication, RepairApplicationAdmin)
admin.site.register(RepairComplaint, RepairComplaintAdmin)
