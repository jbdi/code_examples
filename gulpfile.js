var gulp         = require('gulp'),
    livereload   = require('gulp-livereload'),
    if_          = require('gulp-if'),
    plumber      = require('gulp-plumber'),
    stylus       = require('gulp-stylus'),
    filter       = require('gulp-filter'),
    autoprefixer = require('gulp-autoprefixer'),
    csso         = require('gulp-csso'),
    imagemin     = require('gulp-imagemin'),
    svg2png      = require('gulp-svg2png'),
    notify       = require('gulp-notify'),
    newer        = require('gulp-newer'),
    nib          = require('nib'),
    replace      = require('gulp-replace'),
    rename       = require('gulp-rename'),
    es           = require('event-stream'),
    count        = require('gulp-count'),
    debug        = require('gulp-debug'),
    size         = require('gulp-size'),
    runSequence  = require('run-sequence'),
    baseimg      = require('gulp-baseimg'),
    juice        = require('gulp-juice'),
    sourcemaps   = require('gulp-sourcemaps');

var minimist = require('minimist');
var knownOptions = { Boolean: 'live', default: { live: false } };
var options = minimist(process.argv.slice(2), knownOptions);

function styles(src, dst, use_nib) {
    return function () {
        return gulp.src(src)
            .pipe(plumber())
            .pipe(if_(process.env.NODE_ENV !== 'production', sourcemaps.init()))
            .pipe(stylus({
                'include css': true,
                url: {
                    name: 'embedurl'
                },
                use: use_nib ? [nib()] : []
            }))
            .pipe(autoprefixer('last 2 version', 'ie >= 8', 'opera >= 12.1', 'ios >= 6', 'android >= 4'))
            .pipe(csso())
            .pipe(if_(process.env.NODE_ENV !== 'production', sourcemaps.write('.')))
            .pipe(gulp.dest(dst))
            .pipe(filter("**/*.css"))
            .pipe(if_(options.live, livereload()))
            .pipe(size({title: '(style) size:', showFiles: true}))
            .pipe(notify({message: 'Styles task complete'}));
    }
}

function images(src, dst) {
    return function () {
        return gulp.src(src + '/*.svg')
            .pipe(count('(images) svg files found'))
            .pipe(newer({dest: dst, ext: '.png'}))
            .pipe(count('(images) svg files to convert'))
            .pipe(imagemin({
                svgoPlugins: [
                    {removeViewBox: true}
                ]
            }))
            .pipe(gulp.dest(dst))
            .pipe(svg2png(/*scale=*/undefined, /*verbose=*/false, /*concurrency=*/4))
            .pipe(imagemin())
            .pipe(gulp.dest(dst))
            .pipe(if_(options.live, livereload()))
            .pipe(size({title: '(images) size:'}))
            .pipe(notify({message: 'Images task complete', onLast: true}));
    }
}

function images6(src, dest, styleTemplate, styleName) {
    return function() {
        return gulp.src(src)
            .pipe(count({title: '(images6) svg files to inject'}))
            .pipe(imagemin({
                svgoPlugins: [
                    { removeViewBox: true }
                ]
            }))
            .pipe(plumber())
            .pipe(baseimg({
                styleTemplate: dest + styleTemplate,
                styleName: styleName
            }))
            .pipe(replace("'url(", "url('"))
            .pipe(replace(")'", "')"))
            .pipe(gulp.dest(dest))
            .pipe(size({title: '(images6) size:', showFiles: true}))
            .pipe(notify({
                title: 'Icons',
                message: styleName + ' compile complete'
            }));
    }
}

function svg2png6(src, dest) {
    return function() {
        return gulp.src(src)
            .pipe(count({title: '(svg2png6) svg files found'}))
            .pipe(newer({dest: dest, ext: '.png'}))
            .pipe(count({title: '(svg2png6) svg files to convert'}))
            .pipe(imagemin({
                svgoPlugins: [
                    { removeViewBox: true }
                ]
            }))
            .pipe(svg2png(/*scale=*/undefined, /*verbose=*/false, /*concurrency=*/4))
            .pipe(imagemin())
            .pipe(debug({title: '(svg2png6) converted:'}))
            .pipe(gulp.dest(dest))
            .pipe(size({title: '(svg2png6) size:'}))
            .pipe(notify({
                title: 'Images',
                message: 'PNG build complete',
                onLast: true
            }));
    }
}

function board_navi(src, dst, groups) {
    return function () {
        var pipes = groups.map(function (group) {
            return gulp.src(src)
                .pipe(plumber())
                .pipe(replace("__NAME__", group))
                .pipe(stylus({
                    'include css': true,
                    'resolve url': true,
                    url: {
                        name: 'embedurl'
                    }
                }))
                //.pipe(rename(board_group + "_navimenu.css"));
                .pipe(rename(function(path) {
                    path.basename = group + '_' + path.basename;
                }));
        });

        return es.merge(pipes)
            .pipe(plumber())
            .pipe(count({title: '(board_navi) boards found'}))
            .pipe(autoprefixer('last 2 version', 'ie >= 8', 'opera >= 12.1', 'ios >= 6', 'android >= 4'))
            .pipe(csso())
            .pipe(gulp.dest(dst))
            .pipe(filter("**/*.css"))
            .pipe(if_(options.live, livereload()))
            .pipe(size({title: '(board_navi) size:', showFiles: true}))
            .pipe(notify({message: 'Styles for board navigation generated', onLast: true}));
    }
}

// 2016 layout paths
var src6 = {
    'dev': {
        'stylus': 'layout-6/css/',
        'stylus_boards': 'layout-6/css_boards/',
        'mustaches': 'layout-6/css/mustaches/',
        'mustaches_boards': 'layout-6/css_boards/mustaches/',
        'js': 'layout-6/js/',
        'svg':  'layout-6/i/svg/',
        'svg_boards':  'layout-6/i/svg/board-icons/'
    },
    boards: {
        navimenu: ['cars', 'moto', 'cargo', 'machines', 'waterborne', 'parts',
            'autogoods', 'garages', 'rental', 'job', 'not_found', 'index'],
        iconset: ['choose']
    },
    'production': {
        'css': 'www/media/build-6/css/',
        'js': 'www/media/build-6/js/',
        'images':  'www/media/build-6/i/'
    }
};

gulp.task('images', images('www/media/i/svg-icons', 'www/media/i/svg-icons/build'));
gulp.task('images_mobile', images('www/media/mobile/i/svg-icons', 'www/media/mobile/i/svg-icons/build'));
gulp.task('styles', ['images'], styles('www/media/css/handy.styl', 'www/media/css/', false));
gulp.task('styles_mobile', ['images_mobile'], styles('www/media/mobile/css/style.styl', 'www/media/mobile/css/', true));
gulp.task('styles_print', styles('www/media/css/print.styl', 'www/media/css/', false));
gulp.task('styles_video', styles('www/media/css/video.styl', 'www/media/css/', true));

gulp.task('images6', images6(src6.dev.svg + '*.svg', src6.dev.mustaches,
    'images-template.styl.mustache', '_images-result.styl'));
gulp.task('images6board', images6(src6.dev.svg_boards + '*.svg', src6.dev.mustaches_boards,
    'images-template.styl.mustache', '_images-result.styl'));

gulp.task('svg2png6', svg2png6(src6.dev.svg + '*.svg', src6.production.images));
gulp.task('svg2png6board', svg2png6(src6.dev.svg_boards + '*.svg', src6.production.images + 'board-icons'));

gulp.task('styles6_only', styles(src6.dev.stylus + 'styles.styl', src6.production.css, false));
gulp.task('styles6', function(callback) {
    return runSequence(
        ['images6', 'svg2png6'],
        'styles6_only',
        callback
    )
});

gulp.task('styles6_forum', styles(src6.dev.stylus + 'forum.styl', src6.production.css, false));

// boards navigation menus
gulp.task('styles6board_navimenu', board_navi(src6.dev.stylus_boards + 'navimenu.styl', src6.production.css, src6.boards.navimenu));
gulp.task('styles6board_iconset', board_navi(src6.dev.stylus_boards + 'iconset.styl', src6.production.css, src6.boards.iconset));
gulp.task('styles6board', function(callback) {
    return runSequence(
        ['images6board', 'svg2png6board'],
        ['styles6board_navimenu', 'styles6board_iconset'],
        callback
    )
});


gulp.task('mail_letter', function () {
    gulp.src('templates/email/base_next.html')
        .pipe(juice({
            removeStyleTags: false
        }))
        .pipe(rename(function (path) {
            path.basename += '_inline';
        }))
        .pipe(gulp.dest('./templates/email'));
});


gulp.task('watch', function() {
    if(options.live) livereload.listen();

    gulp.watch('www/media/css/handy/*.styl',         ['styles']);
    gulp.watch('www/media/css/reset.styl',           ['styles']);
    gulp.watch('www/media/css/handy.styl',           ['styles']);
    gulp.watch('www/media/css/print.styl',           ['styles_print']);
    gulp.watch('www/media/mobile/css/*.styl',        ['styles_mobile']);
    gulp.watch('www/media/mobile/css/styles/*.styl', ['styles_mobile']);
    gulp.watch('www/media/css/video.styl',           ['styles_video']);
    gulp.watch('www/media/i/svg-icons/*.svg',        ['images']);
    gulp.watch('www/media/mobile/i/svg-icons/*.svg', ['images_mobile']);

    gulp.watch([src6.dev.stylus + '*.styl', src6.dev.stylus + 'styles/*.styl'], ['styles6_only']);
    gulp.watch(src6.dev.stylus_boards + 'navimenu.styl', ['styles6board_navimenu']);
    gulp.watch(src6.dev.stylus_boards + 'iconset.styl', ['styles6board_iconset']);

    gulp.watch(src6.dev.svg + '*.svg', ['svg2png6', 'styles6']);
    gulp.watch(src6.dev.svg_boards + '*.svg', ['styles6board', 'svg2png6board']);
    gulp.watch('templates/email/base_next.html', ['mail_letter']);
    // gulp.watch(src6.dev.js + '*.js', ['js']);
});

gulp.task('default', [
    'styles',
    'styles_print',
    'styles_mobile',
    'styles_video',

    'styles6',
    'styles6board',

    'mail_letter'
]);
