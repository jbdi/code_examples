# encoding: utf-8
from __future__ import unicode_literals
from datetime import datetime, timedelta

from django.contrib.auth import get_user_model

import timezone

from funcy import *
from funcy.flow import joining
from handy.text import formatnumber

from django.conf import settings
from django.db import models, transaction
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.models import ContentType
from django.utils.text import Truncator


import counters
import redis

from geo.models import City, Region
from handy.models import BigIntegerField
from handy.utils import get_or_none
from timezone.models import TimezoneDateTimeField
from catalog.models import Brand, Model
import cars
from mediavault.models import VaultImageField
from postie.shortcuts import queue_email
from pm import send_system_message, send_system_pm

from schema.fields import *
from schema.decorators import *
from schema.models import FlexModel, FlexCategory
from schema.filters import Chosen
from schema.rules import rule, test, clean
from schema.js import m2m_contains

from board.models.advert.base.fields import *
from board.schema import data
from board.models.advert.car.fields import CarBrandField, CarModelField, EngineField,\
                                           EngineModelField, GearField
from board.schema.filters import GeoCity

from business import RepairAccess
import cars

from repair.fields import (ServiceCategoryField, BrandModelField, ServiceTypeField,
                           BrandModelTitleField, ServiceTypes, LifetimeField, PhonesField,
                           BrandCountryField, BrandField, BrandFilterField, LocationField,
                           RepairEngineFuelField, m2m_contain_service_car)
from repair.filters import BrandTypes, CompanyCountries, REPAIR_CAR_SERVICE_ID

from people.phones import format_phone


active_special = Q(time_special_ends__isnull=False)
paid_region = Q(region__code__in=RepairAccess.paid_regions)
has_repair_access = Q(blocked=False) & (Q(user__repair_access=True) & paid_region | ~paid_region)

redis_conn = redis.Redis(**settings.REPAIR_LABEL_NEW_APPLICATIONS_REDIS)


class Offer(object):
    PENDING = 1
    ACCEPTED = 2
    REJECTED = 3

    choices = (
        (PENDING, 'pending'),
        (ACCEPTED, 'accepted'),
        (REJECTED, 'rejected'),
    )
    work_duration_choices = (
        (1,     '1 час'),
        (2,     '2 часа'),
        (3,     '3 часа'),
        (6,     '6 часов'),
        (1*24,  '1 день'),
        (2*24,  '2 дня'),
        (3*24,  '3 дня'),
        (4*24,  '4 дня'),
        (5*24,  '5 дней'),
        (6*24,  '6 дней'),
        (7*24,  '1 неделя'),
        (14*24, '2 недели'),
        (21*24, '3 недели'),
        (30*24, '1 месяц'),
        (60*24, '2 месяца'),
    )
    work_durations_dict = dict(work_duration_choices)


class Application(object):
    PENDING = 1
    DELETED = 2
    COMPLETED = 3

    choices = (
        (PENDING, 'pending'),
        (DELETED, 'deleted'),
        (COMPLETED, 'completed'),
    )

    lifetime_choices = (
        (1, '1 день'),
        (3, '3 дня'),
        (5, '5 дней'),
        (7, 'Неделя'),
        (14, '2 недели'),
        (21, '3 недели'),
    )


class ServiceCategory(FlexCategory):
    visible = models.BooleanField(u'Видна', default=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Категория услуг'
        verbose_name_plural = 'Категории услуг'


def company_exists(self, value):
    ct = ContentType.objects.get(model='repaircompany')
    ct_repaircompany = ct.model_class()
    return not ct_repaircompany.objects.filter(title=self.title, address=self.address).exclude(pk=self.pk).exists()


from __builtin__ import map


@counters.add('hits')
class RepairCompany(FlexModel):
    title = StringField(
        title           = 'Название',
        clean           = clean.trim,
        show            = 'form.general.5',
        required        = True,
        rules           = [[
            test.re(r"^[0-9a-zA-Zа-яА-ЯёЁ]"),
            "Название компании должно начинаться с буквы или цифры"
        ], [
            test.re(r"^[0-9a-zA-Zа-яА-ЯёЁ+-.,:'/ ]+$"),
            "Название компании может содержать только буквы, цифры, пробелы и знаки + - , . : ' /"
        ], [
            company_exists,
            "Эта компания по указанному адресу уже на кого-то зарегистрирована"
        ]])
    user = OneToOneField(
        title           = 'Представитель компании',
        rel             = settings.AUTH_USER_MODEL,
        fill            = lambda request: request.user)
    description = TextField(
        title           = 'Описание',
        show            = 'form.general',
        required        = False)
    schedule = TextField(
        title           = 'Время работы',
        show            = 'form.general',
        required        = False,
        help_text       = 'Например: Пн-Сб: с 10 до 20')
    phones = PhonesField()
    url_link = StringField(
        title           = 'Ссылка на сайт',
        show            = 'form.general',
        max_length      = 255,
        required        = False,
        default         = '',
        clean           = clean.url)
    email = StringField(
        title           = 'Email',
        show            = 'form.general',
        required        = False,
        null            = True,
        default         = '',
        clean           = clean.trim,
        rules           = [
            [test.opt(test.email), 'Укажите правильный адрес электронной почты'],
        ])
    time_registered = DateTimeField(
        title           = 'Зарегистрирован',
        default         = datetime.now)
    time_updated = DateTimeField(
        title           = 'Изменен',
        default         = datetime.now,
        calc            = lambda self: datetime.now())
    time_be_first_expired = DateTimeField(
        title           = 'Время истечения услуги «Будь первым»',
        null            = True,
        required        = False)
    be_first_activated = BooleanField(
        title           = 'Услуга «Будь первым» активирована',
        default         = False)
    time_special_ends = DateTimeField(
        title           = 'Время окончания услуги Спецпредложение',
        null            = True,
        required        = False)
    dgis_id = BigIntegerField(
        title           = '2GIS ID',
        null            = True,
        required        = False,
        db_index        = True)
    services = ManyToManyField(
        rel             = 'repair.servicecategory',
        title           = 'Оказываемые услуги',
        show            = 'form.services',
        queryset        = lambda field: ServiceCategory.objects.filter(visible=True),
        # Required в схеме чекается при валидации модели, что для m2m-поля смысла не имеет.
        # Поэтому пишем rule для валидации на стороне формы и оставляем required,
        # чтобы работало в админке (ибо required транслируется в джанговский blank).
        rule            = [lambda raw_value: len(raw_value), 'Обязательное поле'],
        required        = True)
    service_type = BitMaskField(
        title           = 'Категории услуг',
        null            = False,
        default         = 0,
        required        = False,
        widget          = 'grouped_bitmask',
        choices         = ServiceTypes.choices,
        show            = 'form.services',
        visible         = depends('services')(m2m_contain_service_car()))
    blocked = BooleanField(
        title           = 'Заблокирована',
        default         = False)

    # Контекст добавления
    host_added = HostAddedField()
    site_region = SiteRegionField()
    ip = IPField()

    # География
    city = CityField(show='form.general.0 list')
    region = RegionField()

    # Brands
    brand_type = IntegerField(
        visible         = depends('services')(m2m_contain_service_car()),
        null            = True,
        required        = False,
        widget          = 'select',
        show            = 'form.general',
        title           = 'Обслуживаемые бренды',
        choices         = BrandTypes.choices)
    brand_country = BrandCountryField()
    brand = BrandField()
    brand_filter = BrandFilterField()

    address = StringField(
        title           = 'Адрес',
        clean           = clean.trim,
        show            = 'form.general.10',
        required        = True,
        help_text       = 'Укажите адрес сервиса',
        widget          = 'map_depended_input')
    location = LocationField(
        show            = 'form.general.20',
        db_index        = True)
    subscribe_email = StringField(
        title           = 'Подписка на новые заявки',
        required        = False,
        null            = True,
        default         = '',
        show            = 'form.general',
        clean           = clean.trim,
        rules           = [
            [test.opt(test.email), 'Укажите правильный адрес электронной почты'],
        ])

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        old_instance = get_or_none(RepairCompany, id=self.pk)
        recipient = self.user.email or self.email
        block_state_changed = old_instance and old_instance.blocked != self.blocked

        state = 'заблокирована' if self.blocked else 'разблокирована'
        context = {
            'subject_ending': 'Ваша компания %s.' % state,
            'title': 'Ваша компания «%s» %s' % (self.title, state),
            'text': 'Ваши предложения на заявки не будут видны заказчикам.\n'
                    'Во время блокировки вы не можете делать предложения по заявкам на ремонт и отвечать на вопросы.\n'
                    'Для разблокировки и установления причин блокировки обратитесь в администрацию сайта.'
        }

        if not self.blocked:
            context['text'] = 'Теперь вам вновь доступна возможность размещать предложения по заявкам и задавать вопросы.'

        if block_state_changed:
            system_message = "%s.\n\n%s" % (context['title'], context['text'])
            send_system_pm(self.user, system_message)
            if recipient:
                queue_email(recipient, template='email/repair/changed_company_state.j2', data=context)
        super(RepairCompany, self).save(*args, **kwargs)


    class Meta:
        verbose_name = 'Автосеравис'
        verbose_name_plural = 'Автосервисы'
        unique_together = ('title', 'address')
        db_table = 'repair_company'
        ordering = ('title',)

    def get_servicing_brands(self):
        cars_service_filter = Q(visible=True, repaircompany__id=self.pk, pk=REPAIR_CAR_SERVICE_ID)
        have_cars_service = ServiceCategory.objects.filter(cars_service_filter).exists()
        if not have_cars_service:
            return []

        countries = []
        brands = []
        if self.brand_type == BrandTypes.ALL_BRANDS:
            countries = ['Обслуживаем все бренды']

        elif self.brand_type == BrandTypes.BRANDS_BY_COUNTRY and self.brand_country:
            countries = [data[1] for key, data in CompanyCountries.DATA_DICT.items()
                         for country_key in self.brand_country if int(country_key) == key]

        elif self.brand_type == BrandTypes.SELECTED_BRANDS:
            brands = Brand.objects.filter(repaircompanies__brand_type=BrandTypes.SELECTED_BRANDS,
                                          repaircompanies__pk=self.pk) \
                                  .values_list('title', flat=True)
            brands = list(brands)
        return countries + brands

    def unique_error_message(self, model_class, unique_check):
        if model_class == type(self) and unique_check == ('title', 'address'):
            return 'Эта компания уже зарегистрирована.'
        else:
            return super(RepairCompany, self).unique_error_message(model_class, unique_check)

    def get_absolute_url(self, request=None):
        return reverse('repair_company_detail', args=[self.id])

    def get_services_tree(self):
        return [
            (service.title, get_chosen_types(get_selected_service_type_choices(self.service_type)))
            if service.label == 'repairapplication.cars' and self.service_type else (service.title, None)
            for service in self.services.all()
        ]

    def get_services_display(self):
        return '. '.join(title for title, _ in self.get_services_tree())

    @property
    def phones_list(self):
        if self.phones:
            return map(lambda x: format_phone(x.strip()), self.phones.split(','))
        else:
            return []

    def get_phones_display(self):
        return ', '.join(self.phones_list)

    def get_seo_title(self):
        services = ', '.join(s.replace('Ремонт ', '') for s in self.services.values_list('title', flat=True))
        return '%s - ремонт %s в Красноярске' % (unicode(self), services)

    @joining('. ')
    def get_seo_description(self):
        for title, services in self.get_services_tree():
            if services:
                yield title + ': ' + ', '.join(services)
            else:
                yield title


@counters.add('hits')
class RepairApplication(FlexModel):
    category = ServiceCategoryField()
    user = UserField()
    time_added = DateTimeField(
        title           = 'Добавлено',
        default         = datetime.now,
        rules           = [
            test.immutable,
            test.le(timezone.now),
        ])
    time_life_started = DateTimeField(
        title           = 'Добавлено',
        db_index        = True,
        sortable        = 'По дате',
        sort_directions = (('desc', 'сначала новые'), ('asc', 'сначала старые')),
        show            = 'sorting.140',
        default         = datetime.now,
        rules           = [
            test.non_decreasing,
            test.ge_field('time_added'),
        ])
    time_life_ended = DateTimeField(
        title           = 'Дата протухания заявки',
        rule            = test.ge_field('time_life_started'),
        calc            = lambda self: self.time_life_started + timedelta(self.lifetime),
        null            = True,
    )
    lifetime = LifetimeField(choices=Application.lifetime_choices)
    reopen_count = PositiveIntegerField(default=0, rule=test.non_decreasing, required=False)
    service_type = ServiceTypeField(category='cars')
    model_lookup = ModelLookupField(category='cars')
    brand = CarBrandField(
        null            = True,
        category        = 'cars')
    model = CarModelField(
        null            = True,
        category        = 'cars')
    brand_model = BrandModelField(
        category        = 'cars',
        show            = 'filter.general.90')
    title = BrandModelTitleField(
        title           = 'Модель',
        show            = 'form.general.40')
    year_made = YearMadeField(show='form.general.150 filter.general.150')
    vin = VinField(category='cars')
    engine = EngineField(category='cars')
    engine_fuel = RepairEngineFuelField(category='cars')
    transmission = TransmissionField(
        category        = 'cars',
        show            = 'form.equipment filter.engine.145',
        choices         = cars.TRANSMISSIONS_WO_SAT,
        filter_choices  = cars.TRANSMISSION_CHOICES_WO_SAT,
        compute_choices = depends('model')(data.model_based_choices_maker('car', 'transmission')))
    gear = GearField(
        category        = ('cars'),
        compute_choices = depends('model')(data.model_based_choices_maker('car', 'gear')))
    description = DescriptionField()
    photos = PhotosField(
        show            = 'form.more.110 filter.general.140.3',
        type_params     = {'profile': 'board', 'max_images': 10})
    video = StringField(
        title           = 'Видео',
        help_text       = 'Укажите ссылку на ролик на youtube, rutube, video.mail.ru, smotri.com, vimeo.com',
        show            = 'form.more.110',
        type_params     = {'max_length': 255},
        null            = True,
        clean           = clean.trim,
        rules           = [
            rule.max_len(255),
            rule.url,
            rule.videohost,
            rule.video_url,
        ])
    state = IntegerField(choices=Application.choices, default=Application.PENDING)
    state_latest_change = DateTimeField(
        title           = 'Дата изменения статуса',
        null            = True,
    )

    # Контекст добавления
    host_added = HostAddedField()
    site_region = SiteRegionField()
    ip = IPField()

    # География
    city = CityField()
    district = DistrictField()
    region = RegionField()

    category_cls = ServiceCategory
    catalog = cars

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        old_instance = get_or_none(RepairApplication, id=self.pk)
        if not old_instance:
            return super(RepairApplication, self).save(*args, **kwargs)

        if old_instance.state != self.state:
            self.state_latest_change = timezone.now()

        return super(RepairApplication, self).save(*args, **kwargs)

    def remove_label_new(self, company_id):
        redis_conn.srem('repaircompany:%s' % company_id, self.id)

    def add_label_new_to_companies(self, companies_id):
        if companies_id:
            pipe = redis_conn.pipeline(transaction=False)
            for company_id in companies_id:
                pipe.sadd('repaircompany:%s' % company_id, self.id)
            pipe.execute()

    def is_new_for_company(self, user):
        if user.is_repair_company:
            return str(self.id) in redis_conn.smembers('repaircompany:%s' % user.repaircompany.id)
        else:
            return None

    @property
    def time_left(self):
        return self.time_life_ended - timezone.now()

    @property
    def days_left(self):
        return self.time_left.days

    @property
    def completed(self):
        return self.state == Application.COMPLETED

    @property
    def accepted_offer(self):
        try:
            return self.current_offers.filter(state=Offer.ACCEPTED).get()
        except RepairCompanyOffer.DoesNotExist:
            return None

    @property
    def current_offers(self):
        return self.offers.filter(created__gt=self.time_life_started)

    @property
    def can_reopen(self):
        """Можно ли перевыставить заявку и с какой задержкой"""
        if self.state in (Application.PENDING, Application.DELETED):
            return False, None

        offer = self.accepted_offer

        if offer is not None:
            yesterday = timezone.now() - timedelta(1)

            if offer.state_updated > yesterday:
                return True, offer.state_updated - yesterday

        return True, None

    @property
    def offers_count(self):
        return self.current_offers.cache().count()

    def have_company_offer(self, user_company):
        return self.current_offers.cache().filter(company=user_company).count()

    def get_transmission_short(self):
        return self.catalog.TRANSMISSIONS_SHORT.get(self.transmission)

    def get_gear_short(self):
        return self.catalog.GEARS_SHORT.get(self.gear, '')


    # NOTE: Запихать сюда нужные поля по мере их появления
    @joining(u', ')
    def short_desc(self, price=True):
        if self.gear:               yield self.get_gear_short()
        if self.transmission:       yield self.get_transmission_short()

    @transaction.atomic
    def complete(self):
        for offer in self.current_offers.exclude(state=Offer.ACCEPTED):
            offer.reject()
        self.state = Application.COMPLETED
        self.save()

    @models.permalink
    def get_absolute_url(self, request=None):
        return 'repair_application_detail', (self.pk,)

    class Meta:
        verbose_name = 'Заявка на ремонт'
        verbose_name_plural = 'Заявки на ремонт'
        db_table = 'repair_application'
        ordering = ('-id',)

    @joining(' ')
    def get_service_type_diff_display(self, previous_service_type):
        def _get_choices_display(title, choices):
            return '%s: %s.' % (title, ', '.join(item.lower() for index, item in ServiceTypes.choices \
                    if index in choices))

        current_service_types = get_selected_service_type_choices(self.service_type)
        previous_service_types = get_selected_service_type_choices(previous_service_type)

        added_choices = current_service_types - previous_service_types
        if added_choices:
            yield _get_choices_display('Добавлено', added_choices)

        deleted_choices = previous_service_types - current_service_types
        if deleted_choices:
            yield _get_choices_display('Удалено', deleted_choices)

    def get_engine_display(self):
        return ', '.join(keep([self.engine, self.get_engine_fuel_display()]))

    def _get_some_seo(self, max_services=None):
        prefix = 'ремонт'

        if self.category.label == 'repairapplication.cars':
            if max_services is None:
                prefix = self.get_service_type_display()
            else:
                if self.service_type and len(get_selected_service_type_choices(self.service_type)) <= max_services:
                    prefix = self.get_service_type_display()

        return 'Требуется %s для %s' % (prefix, self.title)

    def get_seo_title(self):
        return '%s %s' % (self._get_some_seo(5), self.city.get_title_locative())

    def get_seo_description(self):
        return '%s %s' % (self._get_some_seo(), self.city.get_title_locative())

    def get_seo_h1(self):
        return self._get_some_seo(3)

    def get_service_type_display(self):
        choices = get_selected_service_type_choices(self.service_type)
        return concatinate_words(get_chosen_types(choices))


def get_companies_id(region_code):
    return RepairCompany.objects.filter(user__repaircompany__isnull=False, region__code=region_code) \
                                .values_list('id', flat=True)

@receiver(post_save, sender=RepairApplication, dispatch_uid="set_app_new_stage")
def set_app_new_stage(sender, **kwargs):
    if not kwargs.get('created'):
        return
    application = kwargs['instance']
    companies = get_companies_id(application.region_code)
    application.add_label_new_to_companies(companies)

def get_subscribers(region_code):
    users_ids = RepairCompany.objects.filter(has_repair_access, region__code=region_code) \
                                     .exclude(subscribe_email='') \
                                     .values_list('user', flat=True)
    return get_user_model().objects.filter(id__in=users_ids)

@receiver(post_save, sender=RepairApplication, dispatch_uid="send_subscribe_emails")
def send_subscribe_emails(sender, **kwargs):
    if not kwargs.get('created'):
        return

    def detect_user_host(user):
        host = user.repaircompany.host_added or '24auto.ru'
        return host.replace('www.', '')

    application = kwargs['instance']
    users = get_subscribers(application.region_code)
    users_hosts = {user.id: detect_user_host(user) for user in users}
    mail_template = 'email/repair/new_application.j2'
    queue_email(recipients=users, template=mail_template, data={
        'application': application,
        'users_hosts': users_hosts,
    })


def get_chosen_types(choices):
        return [item.lower() for index, item in ServiceTypes.choices if index in choices]

def concatinate_words(words):
    if len(words) <= 2:
        return ' и '.join(words)
    return '%s, %s' % (', '.join(words[:-2]), ' и '.join(words[-2:]))

def get_selected_service_type_choices(service_types):
    return set(keep(int(item) * 2**index \
            for index, item in enumerate(bin(service_types)[:1:-1])))


class RepairCompanyOffer(models.Model):
    company = models.ForeignKey(RepairCompany, related_name='offers')
    application = models.ForeignKey(RepairApplication, related_name='offers')
    price = models.PositiveIntegerField(u'Цена работ, руб.')
    work_duration = models.PositiveSmallIntegerField(u'Максимальный срок выполнения работ',
                                                     choices=Offer.work_duration_choices)
    comment = models.TextField(u'Комментарий', blank=True)
    state = models.IntegerField('Текущее состояние заявки', default=Offer.PENDING)
    created = models.DateTimeField('Дата создания', auto_now_add=True)
    state_updated = models.DateTimeField('Время изменения состояния', auto_now_add=True)

    def __init__(self, *args, **kwargs):
        super(RepairCompanyOffer, self).__init__(*args, **kwargs)
        self._old_state = self.state

    def save(self, *args, **kwargs):
        if self.state != self._old_state:
            self.state_updated = timezone.now()
        super(RepairCompanyOffer, self).save(*args, **kwargs)

    @property
    def terms(self):
        # todo использовать get_work_duration_display
        return Offer.work_durations_dict[self.work_duration]

    @property
    def is_accepted(self):
        return self.state == Offer.ACCEPTED

    @transaction.atomic
    def accept(self):
        offers = self.application.current_offers.select_for_update().all()
        if any(offer.state == Offer.ACCEPTED for offer in offers):
            raise RepairOfferDoubleAccept("You can accept only one offer in application.")
        self.state = Offer.ACCEPTED
        self.save()
        self.application.complete()

    def reject(self):
        self.state = Offer.REJECTED
        self.save()

    def get_thread_id(self):
        # R: много выборок будет на странице заявок.
        return '-'.join(map(str, [
            self.company.user.id,
            self.application.id,
            ContentType.objects.get_for_model(self.application).id]))

    def get_service_thread_id(self):
        return '%d-%d' % (self.application.id, ContentType.objects.get_for_model(self.application).id)

    class Meta:
        db_table = 'repair_offer'


class RepairOfferDoubleAccept(Exception):
    pass


class RepairComplaint(models.Model):
    company = models.ForeignKey(RepairCompany, verbose_name='Компания')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, verbose_name='Автор жалобы')
    author_ip = models.GenericIPAddressField('IP-адрес автора')
    content = models.TextField('Содержание жалобы')
    created = models.DateTimeField('Добавлено', auto_now_add=True)

    def __unicode__(self):
        return Truncator(self.content).words(num=5)

    class Meta:
        db_table = 'repair_complaint'
        verbose_name = 'жалоба на компанию'
        verbose_name_plural = 'жалобы на компании'
        ordering = '-created',


from badges.models import BadgeNubmer
def get_repair_notifications_count(user):
    if user.is_repair_company:
        return BadgeNubmer.get(user, 'repair_company_accepted_offers') \
            + BadgeNubmer.get(user, 'repair_company_rejected_offers')
    else:
        return BadgeNubmer.get(user, 'repair_user_offers')
