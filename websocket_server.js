// WEBSITE_HOST=localhost:8000 ./node_modules/nodemon/bin/nodemon.js websocket_server.js
var sleep = require('sleep');
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(8008, function () {});

app.route('/').options(function(req,res,next){
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET, POST');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        return res.send(200);
    });

app.get('/', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.sendFile(__dirname + '/index.html');
});


/*
  - юзер авторизован
  - проверка стрима на онлайн
  - получение списка непрочитанных донатов
  - включение отображения виджета с донатами
  - синхронизация статусов донатов
*/
var donations = [];
var request = require('request');
var website_host = process.env.WEBSITE_HOST;
var urls = {
  actual_donations: 'http://' + website_host + '/json_actual_donations',
  donations_update_statuses: 'http://' + website_host + '/donations_update_status'
};

var getNewDonations = function (callback) {
  request.get(
    {url: urls.actual_donations, json: true, headers:{'Cache-Control':'no-cache'}},
    function optionalCallback(err, httpResponse, body) {
      if (!err) {
        donations = donations.concat(body);
        if (callback) {
          callback();
        }
      }
    });
};

var donationsUpdateStatuses = function (payload) {
  request.post(payload, function (err, httpResponse, body) {
    if (err) return console.error('upload failed:', err);
  });
};

var csrfPayload = false;

io.on('connection', function (socket) {
  socket.on('set-tokens', function (data) {
     csrfPayload = {csrfmiddlewaretoken: data.csrfmiddlewaretoken}
  });

  socket.emit('queue-donation', { donations: [] });
  socket.on('queue-donation', function (data) {
      if (data && data.donations.length) {
        donationsUpdateStatuses({
          url: urls.donations_update_statuses,
          formData: {donations: JSON.stringify(data.donations)}
        });
      };

      if (!donations.length) {
        getNewDonations(function () {
            socket.emit('queue-donation', { donations: donations });
        });
      } else {
        socket.emit('queue-donation', { donations: [] });
      }
      donations = [];
      sleep.sleep(3);
  });
});




