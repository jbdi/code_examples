# coding: utf-8
from ruauto import render_to
import timezone
from datetime import datetime

from collections import OrderedDict
from operator import attrgetter
from copy import deepcopy

from django.db import transaction
from babel.dates import format_timedelta
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.shortcuts import redirect, get_object_or_404
from django.utils.safestring import mark_safe
from django.contrib import messages
from django.http import Http404
from django.core.urlresolvers import reverse
from django.db.utils import IntegrityError
from django.db.models import Q, Count

from project.decorators import safe_next_url
from handy.ajax import ajax
from handy.pager import pager
from handy.utils import get_or_none
import bans
from coins.services import RepairAccessService
from dgis import api
from postie.shortcuts import queue_email
from pm.views import messages_for_binding
from repair.models import (RepairCompany, RepairCompanyOffer, ServiceCategory,
                           RepairApplication, Application, Offer, RepairComplaint,
                           active_special, has_repair_access, get_companies_id)
from repair.forms import RepairCompanyForm, ApplicationForm, OfferForm
from repair.filters import Filters, CompanyFilters

import business

DEFAULT_IPP = 15
PROMO_COOKIE_NAME = 'repair_promo_visited'


def get_default_city(request):
    if request.geo.city_id and request.geo.city.region == request.region:
        return request.geo.city_id
    return request.region.capital_id


@render_to('repair/application/list.j2')
def index(request):
    repair_user = is_repair_user(request.user)
    promo_page_visited = request.session.get(PROMO_COOKIE_NAME, False)

    if not repair_user and not promo_page_visited:
        return redirect(reverse('repair_promo'))

    filter_defaults = {'region': request.region_code or 24}
    filters = Filters(RepairApplication, request, filter_defaults)
    filters_qs = filters.queryset().nocache().filter(state=Application.PENDING)

    request_order = request.GET.get('order')
    order_types = filters.available_orders.keys()
    ordering = request_order if request_order in order_types else filters.default_order
    page = pager(filters_qs, DEFAULT_IPP, request)

    return {
        'filters': filters,
        # NOTE: коммит с оптимизацией выборки счетчиков предложений #698691b
        'repairapplications': page.object_list,
        'current_ordering': ordering,
        'filtered_applications_count': filters_qs.count(),
        'page': page,
        'show_promo_block': not repair_user,
    }


@render_to()
def promo(request):
    request.session[PROMO_COOKIE_NAME] = 1

    filter_defaults = {'region': request.region_code or 24}
    filters_applications = Filters(RepairApplication, request, filter_defaults)
    filters_companies = CompanyFilters(RepairCompany, request, filter_defaults)
    applications = filters_applications.queryset().nocache().filter(state=Application.PENDING)

    companies = filters_companies.queryset().nocache().filter(has_repair_access)

    return {
        'applications_count': applications.count(),
        'company_count': companies.count()
    }


@render_to('repair/company/detail.j2')
def detail_repair_company(request, repair_company_id):
    repair_company = get_object_or_404(RepairCompany, pk=repair_company_id)

    if repair_company.user != request.user:
        repair_company.incr_hits()

    return {
        'repair_company': repair_company
    }


@login_required()
@render_to('repair/company/form.j2')
def add_repair_company(request):
    if request.user.is_repair_company:
        messages.error(request, 'Нельзя зарегистрировать более одного автосервиса на один аккаунт.')
        return redirect('repair_company_detail', request.user.repaircompany.id)

    if request.user.is_applicant:
        messages.error(request,
                       mark_safe('Вы уже оставляли заявки на ремонт и не можете зарегистрировать автосервис.<br>\
                       Если вы хотите выполнять заявки других пользователей, зарегистрируйте автосервис на другой аккаунт.'))
        return redirect(reverse('my_repair'))

    instance = RepairCompany.instance_for_request(request)
    initial = {'city': get_default_city(request)}

    form = RepairCompanyForm(request.POST or None, initial=initial, instance=instance)

    if form.is_valid():
        form.save()
        messages.success(request, 'Спасибо за регистрацию')
        return redirect('repair_index')


    if request.region.code in business.RepairAccess.paid_regions:
        paid_service = u'<br>Доступ к разделу ремонт для автосервисов <a href="/services/#10">платный</a>.'
    else:
        paid_service = ''

    messages.info(request, mark_safe(
        u'Новая компания будет зарегистрирована и привязана к вашему аккаунту.<br>\
        Примите во внимание: став представителем компании, вы получите возможность<br>\
        предлагать услуги в ответ на заявки, но не сможете сами оставлять заявки.<br>\
        %s' % paid_service))

    return {'form': form}


@render_to('repair/company/list.j2')
def list_repair_companies(request):
    filter_defaults = {'region': request.region_code or 24}
    filters = CompanyFilters(RepairCompany, request, filter_defaults)

    companies = filters.queryset().nocache().filter(has_repair_access)
    specials = filters.queryset().filter(has_repair_access & active_special).order_by('-time_special_ends')
    geo_total_companies = filters.geo_queryset().filter(has_repair_access).count()

    page = pager(companies, DEFAULT_IPP, request)
    return {
        'repair_companies': page.object_list,
        'filters': filters,
        'companies_count': companies.count(),
        'geo_total_companies': geo_total_companies,
        'specials': specials,
        'page': page,
        'show_promo_block': not is_repair_user(request.user),
    }


@render_to('repair/company/special.j2')
def special_repair_companies(request):
    special_query = Q(region__code=request.region.code) & active_special & has_repair_access
    specials = RepairCompany.objects.filter(special_query).order_by('-time_special_ends')
    return {
        'business': business,
        'specials': specials,
    }


@login_required()
@safe_next_url
@render_to('repair/company/form.j2')
def edit_repair_company(request, repair_company_id, next_url=None):
    repair_company = get_object_or_404(RepairCompany, pk=repair_company_id)

    next_page = next_url or reverse('repair_company_detail', args=[repair_company.id])
    if repair_company.user_id != request.user.id:
        return {'message': u'Это не ваша компания.', 'TEMPLATE': 'error.j2'}

    form = RepairCompanyForm(request.POST or None, instance=repair_company)
    if form.is_valid():
        form.save()
        return redirect(next_page)
    return {
        'form': form,
        'action': 'edit',
    }


@ajax
@ajax.login_required
@transaction.atomic
def make_offer(request):
    form = OfferForm(request.POST or None)
    if request.user.is_repair_company and request.user.repaircompany.blocked:
        raise ajax.error(u"Ваша компания заблокирована. Вы не можете предлагать услуги.")

    if not request.user.is_active_repair_company:
        raise ajax.error(u"Вы не можете предлагать услуги пока не купите подписку")

    if request.method != 'POST' or 'application' not in request.POST:
        raise ajax.error(u"Неверные параметры запроса")

    if form.is_valid():
        if request.user.repaircompany.region_code != form.instance.application.region_code:
            raise ajax.error(u"Вы не можете предлагать услуги в других регионах")

        if form.instance.application.current_offers.filter(company=request.user.repaircompany).exists():
            raise ajax.error(u"Вы уже делали предложение по этой заявке")

        form.instance.company = request.user.repaircompany
        offer = form.save()

        queue_email(offer.application.user, 'email/repair/inform_user_new_offer.j2', {
            'application_id': offer.application_id,
            'application_title': offer.application.title,
            'offer_price': offer.price,
            'offer_terms': offer.terms,
            'company_id': offer.company_id,
            'company_title': offer.company.title,
        })
        messages.success(request, u'Ваше предложение отправленно заказчику, если оно ему понравится он свяжется с вами.')
    else:
        error_message = {
            'price': u'Укажите цену в рублях',
            'work_duration': u'Укажите примерный срок выполнения работ',
        }

        raise ajax.error(error_message[form.errors.keys().pop()])


@login_required()
@safe_next_url
@transaction.atomic
def delete_offer(request, offer_id, next_url=None):
    offer = get_or_none(RepairCompanyOffer, id=offer_id)

    if not offer:
        messages.success(request, 'Ваше предложение было удалено.')
        return redirect(next_url or reverse('repair_index'))

    if offer.company.user_id != request.user.id:
        messages.error(request, 'Это предложение сделали не Вы. Не вам и удалять.')
        return redirect(next_url or reverse('repair_index'))

    application_id = offer.application.id

    if offer.state == Offer.ACCEPTED:
        messages.error(request, 'Ваше предложение принято заказчиком, вы не можете его удалить')
        url = reverse('my_repair') + '?state=accepted'
    else:
        offer.delete()
        queue_email(offer.application.user, 'email/repair/inform_user_offer_deleted.j2', {
            'application_id': offer.application_id,
            'application_title': offer.application.title,
            'company_id': offer.company_id,
            'company_title': offer.company.title,
        })
        messages.success(request, 'Вы удалили свое предложение')
        url = next_url or reverse('repair_application_detail', kwargs={'application_id': application_id})

    return redirect(url)


@ajax
def get_2gis_company_profile(request):
    id = request.GET.get('id', '')
    if not id or not hash:
        return None

    return api.firm(id=id, gis_uid=request.COOKIES.get('_2gis_user'), gis_sid=request.COOKIES.get('_2gis_session'))


@login_required()
@safe_next_url
@transaction.atomic
@render_to('repair/application/form.j2')
@bans.check('repair')
def add_application(request, next_url=None):
    instance = RepairApplication.instance_for_request(request)

    if RepairCompany.objects.filter(user__id=request.user.id).exists():
        messages.error(request, "Пользователь, зарегистрировавший команию, не может добавлять заявки на выполнение работ.")
        return redirect(next_url or reverse('repair_index'))

    initial = {'city': get_default_city(request)}

    form = ApplicationForm(request.POST or None, initial=initial, instance=instance)

    if form.is_valid():
        application = form.save(commit=False)
        if application.pk:
            application.time_life_started = timezone.now()
        application.save()

        request.user.is_applicant = True
        request.user.save()
        return redirect(reverse('repair_application_add_success') + '?id=%s' % application.pk)

    return {'form': form}


@login_required()
@render_to('repair/application/add_success.j2')
def add_application_success(request):
    if not 'id' in request.GET:
        raise Http404
    application = get_object_or_404(RepairApplication, id=request.GET['id'], user=request.user)
    return {'application': application}


@login_required()
@safe_next_url
@render_to('repair/application/form.j2')
def edit_application(request, application_id, next_url=None):
    application = get_object_or_404(RepairApplication, id=application_id)
    action = 'edit'

    action_message = {
        'edit': 'Ваша заявка изменена',
        'reopen': 'Ваша заявка была перевыставлена',
    }

    if application.user_id != request.user.id:
        return {'message': u'Это не ваша заявка.', 'TEMPLATE': 'error.j2'}

    next_page = next_url or reverse('my_repair')

    if application.state == Application.DELETED:
        messages.error(request, "Заявка завершена и не подлежит редактированию.")
        return redirect(next_page)

    if application.completed:
        can_reopen, delay = application.can_reopen

        if not can_reopen:
            messages.error(request, 'Эту заявку нельзя перевыставить')
            return redirect(next_page)

        if delay:
            delay = format_timedelta(delay, locale='ru_RU').encode('utf-8')
            messages.error(request, 'Эту заявку можно будет перевыставить через %s.' % delay)
            return redirect(next_page)

        action = 'reopen'

    old_application = deepcopy(application)
    form = ApplicationForm(request.POST or None, instance=application)

    if form.is_valid():
        application = form.save(commit=False)
        changable_fields = [f.name for f in application.schema.fields_for_leaf(application.category) \
                if 'form' in f.show]
        # TODO: service_type нужно показывать в совершённых изменениях, но там битмаска,
        #       нужно парсить.
        changed_fields = [name for name in changable_fields \
                if name not in ('photos', 'brand', 'model') \
                and getattr(old_application, name) != getattr(application, name)]
        if changed_fields:
            offers_users = map(attrgetter('company.user'), application.current_offers.all())
            if offers_users:
                queue_email(offers_users, "email/repair/inform_company_application_modified.j2", {
                    'changed_fields': changed_fields,
                    'application': application,
                    'old_application': old_application,
                })


        if action == 'reopen':
            application.time_life_started = timezone.now()
            application.state = Application.PENDING
            application.reopen_count += 1

        application.save()

        companies_id = get_companies_id(application.region_code)
        application.add_label_new_to_companies(companies_id)
        messages.success(request, action_message[action])
        return redirect(next_page)

    return {'form': form, 'action': action}


@login_required()
@transaction.atomic
def delete_application(request, application_id):
    application = get_object_or_404(RepairApplication, id=application_id)

    if application.user_id != request.user.id:
        messages.error(request, "Нельзя удалить чужую заявку.")
        return redirect(reverse('my_repair'))

    if application.accepted_offer:
        messages.error(request, 'Нельзя удалить заявку, для которой было принято предложение от компании.')
        return redirect(reverse('my_repair'))
    application.state = Application.DELETED
    application.save()

    if application.current_offers.filter(state=Offer.PENDING).exists(): # R: проверить на accepted
    # NOTE: не понимаю, зачем проверять на accepted
        offers = application.current_offers.all()
        offers_users = map(attrgetter('company.user'), offers)

        if offers:
            for offer in offers:
                offer.delete()
            messages.success(request, 'Ваша заявка была удалена. Компании сделавшие предложения были уведомлены.')

        queue_email(offers_users, "email/repair/inform_company_application_deleted.j2", {
            'application_title': application.title,
        })
    return redirect(reverse('my_repair'))


@login_required()
@safe_next_url
@transaction.atomic
def accept_company_offer(request, offer_id, next_url=None):
    company_offer = get_object_or_404(RepairCompanyOffer, id=offer_id)
    next_page = next_url or reverse('my_repair')

    if company_offer.application.user_id != request.user.id:
        messages.error(request, 'Это не ваша заявка.')
        return redirect(reverse('my_repair'))

    if company_offer.state != Offer.PENDING:
        return redirect(next_page)

    company_offer.accept()

    application = company_offer.application
    queue_email(company_offer.company.user, "email/repair/inform_company_offer_accepted.j2", {
        'application_id': application.id,
        'application_title': application.title,
        'user': application.user,
    })
    return redirect(next_page)


@render_to()
def detail_application(request, application_id):
    qs = RepairApplication.objects.exclude(state=Application.DELETED)
    application = get_object_or_404(qs, id=application_id)
    questions, question_form = messages_for_binding(application)
    application_owner = application.user == request.user

    if not application_owner and application.state != Application.COMPLETED:
        application.incr_hits()

    pending_company_offer = None
    if request.user.is_repair_company:
        application.remove_label_new(request.user.repaircompany.pk)
        try:
            pending_company_offer = application.current_offers \
                .filter(state=Offer.PENDING, company=request.user.repaircompany).get()
        except RepairCompanyOffer.DoesNotExist:
            pass

    context = {
        'questions': questions,
        'question_form': question_form,
        'application': application,
        'paid_regions': business.RepairAccess.paid_regions,
        'offers_count': application.current_offers.filter(company__blocked=False).count() if application_owner else None,
        'pending_company_offer': pending_company_offer,
        'offer_form': OfferForm(initial={'application': application.pk}),
        'TEMPLATE': 'repair/application/detail.j2'
    }
    if application.state == Application.COMPLETED:
        accepted_offers = application.current_offers.filter(state=Offer.ACCEPTED)
        if accepted_offers:
            context['time_closed'] = accepted_offers[0].state_updated
        context['is_closed'] = True
        context.update(STATUS=410)
    return context


@ajax
def company_subscribe(request):
    context = {'message': ''}
    email = request.POST.get('email')

    if not request.user.is_active_repair_company:
        raise ajax.error("Подписаться может только компания с купленной "
                         "услугой «Подписка на раздел Ремонт».")
    try:
        validate_email(email)
    except ValidationError:
        if email:
            raise ajax.error("Введите правильный адрес электронной почты.")
        raise ajax.error("Введите адрес электронной почты.")

    if email:
        request.user.repaircompany.subscribe_email = email
        request.user.repaircompany.save()
    return {
        'message': 'Вы подписаны на новые заявки.',
        'show_unsubscribe': True,
    }

@ajax
def company_unsubscribe(request):
    request.user.repaircompany.subscribe_email = ''
    request.user.repaircompany.save()
    return {'message': 'Вы отписаны от рассылки новых заявок.'}


@ajax
def company_complaint(request):
    if request.method != 'POST' or any(field not in request.POST for field in ['content', 'company_id']):
        raise ajax.error(u'Неверные параметры запроса')

    content = request.POST.get('content')
    company_id = request.POST.get('company_id')

    if not content:
        raise ajax.error(u'Заполните текст жалобы')

    complaint = RepairComplaint()
    complaint.company_id = company_id
    complaint.content = content
    complaint.author_ip = request.META.get('REMOTE_ADDR', 0)
    complaint.author = request.user if isinstance(request.user, get_user_model()) else None
    complaint.save()
    messages.success(request, 'Ваша жалоба принята и будет рассмотрена в ближайшее время. Спасибо.')


# NOTE: Возможно, лучше будет вынести в monkeypatch
def is_repair_user(user):
    return user.is_repair_company or user.is_applicant
